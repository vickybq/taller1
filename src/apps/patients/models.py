from django.db import models
# from django.core.validators import RegexValidator, MaxValueValidator, MinValueValidator
from django.core.validators import MinValueValidator, MaxValueValidator
# Create your models here.


# Modelo de Telefono relacionado a ManyToMany a phone_number del modelo Patients
class Telephone(models.Model):
    phone = models.IntegerField(validators=[MinValueValidator(10000000), MaxValueValidator(9999999999)], unique=True, verbose_name='Telefono')
    country_code = models.CharField(choices=(('0', 'USA +1'), ('1', 'MX +52')), max_length=2, verbose_name='Codigo de Pais')
    type = models.CharField(choices=(('0', 'Fijo'), ('1', 'Celular')), max_length=2, verbose_name='Tipo de Telefono')

    def __str__(self):
        return self.mask()

    def mask(self):
        # Mascara del número teléfonico.
        lada = self.phone[0:3]
        tres = self.phone[3:6]
        final = self.phone[6:]
        return '%s (%s) %s-%s' %(str(self.get_country_code_display()), lada, tres, final)


# Modelo de Correo relacionado a ManyToMany a emails del modelo Patients
class Email(models.Model):
    email = models.EmailField(max_length=100, verbose_name='Email')
    type_email = models.CharField(choices=(('0', 'Trabajo'), ('1', 'Personal')), max_length=2, blank=False, null=False,
                                  verbose_name='Tipo de Correo')

    def __str__(self):
        return self.email


# Modelo de Genero relacionado a ForeignKey a gender del modelo Patients
class Gender(models.Model):
    name = models.CharField(max_length=10)

    def __str__(self):
        return self.name


# Modelo de pacientes
class Patients(models.Model):
    name = models.CharField(max_length=50, verbose_name='Nombre')
    last_name = models.CharField(max_length=50, verbose_name='Apellido')
    phone_number = models.ManyToManyField(Telephone, through='PatientsTelephone')
    emails = models.ManyToManyField(Email, through='PatientsEmail')
    gender = models.ForeignKey(Gender, blank=False, null=False, on_delete=models.CASCADE)
    notes = models.TextField(blank=True, null=True, verbose_name='Nota')
    identifier = models.CharField(max_length=10, blank=False, null=True)
    active = models.BooleanField(default=True, verbose_name='Activo')

    def full_name(self):
        # Funcion que retorna el nombre y apellido en full_name
        return "{} {}".format(self.name, self.last_name)

    def __str__(self):
        return self.full_name()

    def save(self):
        #Función guardar
        # Se elimina los espacios en blanco al inicio, final y en medio con la funcion join() en el nombre y apellido con la función split()
        self.name = " ".join(self.name.split())
        self.last_name = " ".join(self.last_name.split())
        # Se cambia nombre y apellido a letra capital con funcion title() Pone en mayusculas la primer letra de una cadena
        self.name = self.name.title()
        self.last_name = self.last_name.title()
        super(Patients, self).save()


    class Meta:
        #Se elimina la 's' en el admin
        verbose_name = 'Patient'
        verbose_name_plural = 'Patients'
        db_table = 'patients'


# Tabla que se crea mediante la propiedad 'THROUGH' para relacionar pacientes y telefonos (M2M)
class PatientsTelephone(models.Model):
    patient = models.ForeignKey(Patients, on_delete=models.CASCADE)
    telephone = models.ForeignKey(Telephone, on_delete=models.CASCADE)
    note_phone = models.TextField(blank=True, null=True, verbose_name='Nota')
    priority_phone = models.IntegerField(default=0)

    def __str__(self):
        return self.patient, self.telephone


# Tabla que se crea mediante la propiedad 'THROUGH' para relacionar pacientes y correos (M2M)
class PatientsEmail(models.Model):
    patient = models.ForeignKey(Patients, on_delete=models.CASCADE)
    email = models.ForeignKey(Email, on_delete=models.CASCADE)
    note_email = models.TextField(blank=True, null=True, verbose_name='Nota')
    priority_email = models.IntegerField(default=0)

    def __str__(self):
        return self.patient, self.email
