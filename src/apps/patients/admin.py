from django.contrib import admin
from .models import Patients, Email, Telephone, Gender, PatientsTelephone, PatientsEmail
# Register your models here.
class PatientAdmin(admin.ModelAdmin):
    pass
admin.site.register(Patients, PatientAdmin)


class EmailsAdmin(admin.ModelAdmin):
    pass
admin.site.register(Email, EmailsAdmin)


class TelephoneAdmin(admin.ModelAdmin):
    pass
admin.site.register(Telephone, TelephoneAdmin)


class GenderAdmin(admin.ModelAdmin):
    pass
admin.site.register(Gender, GenderAdmin)


class PatientsTelephoneAdmin(admin.ModelAdmin):
    pass
admin.site.register(PatientsTelephone, PatientsTelephoneAdmin)


class PatientsEmailAdmin(admin.ModelAdmin):
    pass
admin.site.register(PatientsEmail, PatientsEmailAdmin)